﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace eabr_backend.Migrations
{
    public partial class newinit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "invertory_code_seq");

            migrationBuilder.CreateTable(
                name: "InventoryItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: false, defaultValueSql: "trim(to_char(nextval('invertory_code_seq'), '099999999'))"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedById = table.Column<Guid>(nullable: false),
                    PhotoFileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InventoryHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateStart = table.Column<DateTime>(nullable: false),
                    DateEnd = table.Column<DateTime>(nullable: false),
                    InventoryByUserId = table.Column<Guid>(nullable: false),
                    InventoryByItemId = table.Column<Guid>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryHistories_InventoryItems_InventoryByItemId",
                        column: x => x.InventoryByItemId,
                        principalTable: "InventoryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "InventoryItems",
                columns: new[] { "Id", "CreatedById", "Description", "Name", "PhotoFileName" },
                values: new object[,]
                {
                    { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), new Guid("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"), "Персональный компьютер", "ПК HP", null },
                    { new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), new Guid("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"), "Кресло офисное", "Кресло офисное", null },
                    { new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), new Guid("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"), "Монитор Dell 24", "Монитор Dell", null }
                });

            migrationBuilder.InsertData(
                table: "InventoryHistories",
                columns: new[] { "Id", "DateEnd", "DateStart", "InventoryByItemId", "InventoryByUserId", "IsConfirmed", "Remark" },
                values: new object[,]
                {
                    { new Guid("7225e110-399a-45af-a0bc-c07b12093eb7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null },
                    { new Guid("55f6dcaa-2313-4ef2-802e-bba3fb89b796"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_InventoryHistories_Id",
                table: "InventoryHistories",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryHistories_InventoryByItemId",
                table: "InventoryHistories",
                column: "InventoryByItemId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_Code",
                table: "InventoryItems",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_Id",
                table: "InventoryItems",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryHistories");

            migrationBuilder.DropTable(
                name: "InventoryItems");

            migrationBuilder.DropSequence(
                name: "invertory_code_seq");
        }
    }
}
