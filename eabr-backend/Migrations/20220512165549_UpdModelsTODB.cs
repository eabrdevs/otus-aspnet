﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace eabr_backend.Migrations
{
    public partial class UpdModelsTODB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_InventoryItems_Code",
                table: "InventoryItems");

            migrationBuilder.DropIndex(
                name: "IX_InventoryItems_Id",
                table: "InventoryItems");

            migrationBuilder.DropIndex(
                name: "IX_InventoryHistories_Id",
                table: "InventoryHistories");

            migrationBuilder.DeleteData(
                table: "InventoryHistories",
                keyColumn: "Id",
                keyValue: new Guid("55f6dcaa-2313-4ef2-802e-bba3fb89b796"));

            migrationBuilder.DeleteData(
                table: "InventoryHistories",
                keyColumn: "Id",
                keyValue: new Guid("7225e110-399a-45af-a0bc-c07b12093eb7"));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "InventoryItems",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "InventoryHistories",
                columns: new[] { "Id", "DateEnd", "DateStart", "InventoryByItemId", "InventoryByUserId", "IsConfirmed", "Remark" },
                values: new object[,]
                {
                    { new Guid("635b147d-1381-4103-95f8-70d22c4c01e7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null },
                    { new Guid("652ea5c3-3fd5-4545-84ea-0b70160cdfb1"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "InventoryHistories",
                keyColumn: "Id",
                keyValue: new Guid("635b147d-1381-4103-95f8-70d22c4c01e7"));

            migrationBuilder.DeleteData(
                table: "InventoryHistories",
                keyColumn: "Id",
                keyValue: new Guid("652ea5c3-3fd5-4545-84ea-0b70160cdfb1"));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "InventoryItems",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.InsertData(
                table: "InventoryHistories",
                columns: new[] { "Id", "DateEnd", "DateStart", "InventoryByItemId", "InventoryByUserId", "IsConfirmed", "Remark" },
                values: new object[,]
                {
                    { new Guid("7225e110-399a-45af-a0bc-c07b12093eb7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null },
                    { new Guid("55f6dcaa-2313-4ef2-802e-bba3fb89b796"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), new Guid("0e518214-29e7-4801-a775-b5dbc9c6b8c9"), false, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_Code",
                table: "InventoryItems",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_Id",
                table: "InventoryItems",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryHistories_Id",
                table: "InventoryHistories",
                column: "Id",
                unique: true);
        }
    }
}
