﻿using eabr_backend.Models;
using System;
using System.Collections.Generic;

namespace eabr_backend.Data
{
    public static class DefaultData
    {
        public static IEnumerable<InventoryItem> InventoryItems => new List<InventoryItem>()
        {
            new InventoryItem()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Code = null,
                Name = "ПК HP",
                Description = "Персональный компьютер",
                CreatedById = Guid.Parse("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"),
            },
             new InventoryItem()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Code = null,
                Name = "Кресло офисное",
                Description = "Кресло офисное",
                CreatedById = Guid.Parse("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"),
            },
              new InventoryItem()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Code = null,
                Name = "Монитор Dell",
                Description = "Монитор Dell 24",
                CreatedById = Guid.Parse("ef2c1f92-78eb-48e4-b3ca-d861e11a4808"),
            }
          };


        public static IEnumerable<InventoryHistory> InventoryHistories => new List<InventoryHistory>()
            {
                    new InventoryHistory()
                    {
                        Id = Guid.NewGuid(),                        
                        InventoryByItemId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                        InventoryByUserId = Guid.Parse("0e518214-29e7-4801-a775-b5dbc9c6b8c9")

                    },
                    new InventoryHistory()
                    {
                        Id = Guid.NewGuid(),                        
                        InventoryByItemId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                        InventoryByUserId = Guid.Parse("0e518214-29e7-4801-a775-b5dbc9c6b8c9")
                    }
            };
    }
}
