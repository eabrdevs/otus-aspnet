﻿using eabr_backend.GraphQl.InventoryHistories;
using eabr_backend.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.Data.Inventory
{
    public interface IInventoryHistoryRepository
    {
        Task<InventoryHistory> CreateAsync(InventoryHistory history);
        Task<IQueryable<InventoryHistory>> GetInventoryHistoriesAsync();
        Task<IQueryable<InventoryHistory>> GetByUserIdAsync(Guid id);
        Task<IQueryable<InventoryHistory>> GetByIdHistoriesAsync(Guid id);
        Task<InventoryHistory> Confirmed(ConfirmedInput input);
    }
}
