﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eabr_backend.Models;
using Microsoft.EntityFrameworkCore;

namespace eabr_backend.Data.Inventory
{
    public class InventoryRepository : IInventoryRepository
    {
        private readonly DataContext _context;
        public InventoryRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<InventoryItem> CreateAsync(InventoryItem item)
        {            
            _context.InventoryItems.Add(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task<IQueryable<InventoryItem>> GetByIdAsync(Guid id)
        {            
            var item = await Task.Run(() => _context.InventoryItems.Include(h => h.Id==id));
            return item;
        }
        

        public async Task<IQueryable<InventoryItem>> GetInventoriesAsync()
        {            
            return await Task.Run(() => _context.InventoryItems);
        }
    }
}
