﻿using eabr_backend.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.Data.Inventory
{
    public interface IInventoryRepository
    {
        Task<InventoryItem> CreateAsync(InventoryItem item);
        Task<IQueryable<InventoryItem>> GetInventoriesAsync();
        Task<IQueryable<InventoryItem>> GetByIdAsync(Guid id);
    }
}
