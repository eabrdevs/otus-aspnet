﻿using System.Linq;
using eabr_backend.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using eabr_backend.GraphQl.InventoryHistories;

namespace eabr_backend.Data.Inventory
{
    public class InventoryHistoryRepository : IInventoryHistoryRepository
    {
        private readonly DataContext _context;
        public InventoryHistoryRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<InventoryHistory> CreateAsync(InventoryHistory history)
        {
            _context.InventoryHistories.Add(history);
            await _context.SaveChangesAsync();
            return (InventoryHistory)history;
        }

        public async Task<InventoryHistory> Confirmed(ConfirmedInput input)
        {
            var hst = _context.InventoryHistories.Where(h => h.Id==input.Id);
            hst.FirstOrDefault().IsConfirmed = input.IsConfirmed;
            await _context.SaveChangesAsync();
            return hst.FirstOrDefault();
        }

        public async Task<IQueryable<InventoryHistory>> GetByUserIdAsync(Guid id)
        {
            var history = await Task.Run(() => _context.InventoryHistories.Where(u => u.InventoryByUserId == id));
            return history;
        }

        public async Task<IQueryable<InventoryHistory>> GetInventoryHistoriesAsync()
        {
            return await Task.Run(() => _context.InventoryHistories);
        }

        public async Task<IQueryable<InventoryHistory>> GetByIdHistoriesAsync(Guid id)
        {
            return await Task.Run(() => _context.InventoryHistories.Where(i=>i.InventoryByItemId == id));
        }
    }
}
