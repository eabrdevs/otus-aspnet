﻿using eabr_backend.Models;
using Microsoft.EntityFrameworkCore;

namespace eabr_backend.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }        
        /** Инвентарь */
        public DbSet<InventoryItem> InventoryItems { set; get; }
        /** История инвентаря */
        public DbSet<InventoryHistory> InventoryHistories { set; get; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<InventoryItem>()
                .HasMany(i => i.InventoryHistories)
                .WithOne(i => i.InventoryByItem!)
                .HasForeignKey(i => i.InventoryByItemId);
            modelBuilder.HasSequence<int>("invertory_code_seq");
            modelBuilder.Entity<InventoryItem>()
                .Property(o => o.Code)
                .HasDefaultValueSql("trim(to_char(nextval('invertory_code_seq'), '099999999'))");
            modelBuilder
                .Entity<InventoryItem>(ent => ent.HasData(DefaultData.InventoryItems));

            modelBuilder
                .Entity<InventoryHistory>()
                .HasOne(i => i.InventoryByItem)
                .WithMany(i => i.InventoryHistories)
                .HasForeignKey(i => i.InventoryByItemId);
            modelBuilder
                .Entity<InventoryHistory>(ent => ent.HasData(DefaultData.InventoryHistories));
        }
    }
}
