﻿using eabr_backend.Models;
using Microsoft.EntityFrameworkCore;

namespace eabr_backend.Data.IdentityUser
{
    public class UserContext: DbContext
    {
        public UserContext(DbContextOptions<UserContext> options): base(options)
        {
        }
        /** Потльзователи */
        public DbSet<User> Users { set; get; }
    }
}
