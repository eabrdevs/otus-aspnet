﻿using eabr_backend.Models;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.Data.IdentityUser
{
    public class UserRepository : IUserRepository
    {
        private readonly UserContext _context;
        public UserRepository(UserContext context)
        {
            _context = context;
        }

        public IQueryable<User> GetByEmail(string email)
        {
            return _context.Users.Where(u => u.Email == email);
        }

        public async Task<IQueryable<User>> GetByIdAsync(string id)
        {
            var users = await Task.Run(() => _context.Users.Where(u => u.Id == id));
            return users;
        }

        public async Task<IQueryable<User>> GetUserAsync()
        {
            return await Task.Run(() => _context.Users);
        }

    }
}
