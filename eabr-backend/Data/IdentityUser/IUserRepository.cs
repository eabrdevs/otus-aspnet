﻿using eabr_backend.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.Data.IdentityUser
{
    public interface IUserRepository
    {
        Task<IQueryable<User>> GetUserAsync();
        IQueryable<User> GetByEmail(string email);
        Task<IQueryable<User>> GetByIdAsync(string id);
    }
}
