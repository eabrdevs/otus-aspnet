﻿using Microsoft.AspNetCore.Mvc;
using eabr_backend.Data.Inventory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eabr_backend.Helpers;
using eabr_backend.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using eabr_backend.Data.IdentityUser;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eabr_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase
    {       
        private readonly IInventoryRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly IWebHostEnvironment _env;

        public InventoryController(IInventoryRepository repository, IWebHostEnvironment env, IUserRepository userRepository)
        {
            _repository = repository;
            _env = env;
            _userRepository = userRepository;
        }

        /*[HttpPost("add")]
        public IActionResult AddInventory(InventoryItemDto itemDto)
        {
            var item = new InventoryItem
            {
                Id = new Guid(),
                Name = itemDto.Name,
                Description = itemDto.Description,
                CreatedById = itemDto.CreatedById,
                PhotoFileName = itemDto.PhotoFileName
            };

            return Created("success", _repository.Create(item));
        }*/

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + filename;

                using(var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return new JsonResult(filename);
            }
            catch (Exception)
            {
                return new JsonResult("anonymous.png");
            }
        }

        [HttpGet("get")]
        [Authorize]
        public JsonResult Get()
        {
            var inventoryitem = _repository.GetInventoriesAsync().Result;
            return new JsonResult(inventoryitem);
        }
    }
}
