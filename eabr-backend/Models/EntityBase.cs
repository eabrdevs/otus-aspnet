﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eabr_backend.Models
{
    public abstract class EntityBase
    {
        [Key]
        /** Уникальный идентификатор */
        public Guid Id { get; set; }
    }
}
