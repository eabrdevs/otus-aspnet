﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eabr_backend.Models
{
    /// <summary>
    /// Инвентарь
    /// </summary>    
    public class InventoryItem : EntityBase
    {
        [Required]
        [Display(Name = "Инвентарный номер")]
        public string Code { get; set; }       
        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }        
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Пользователь создавший инвентарь")]
        public Guid CreatedById { get; set; }
        //public User User { get; set; }
        public string PhotoFileName { get; set; }
        //public ICollection<User> Users { get; set; } = new List<User>();
        public ICollection<InventoryHistory> InventoryHistories { get; set; } = new List<InventoryHistory>();

    }
}
