﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.Models
{
    public class User
    {
        public string Id { get; set; }
        /** ФИО пользователя */
        public string UserName { get; set; }
        /** Емайл пользователя */
        public string Email { get; set; }
        public string PhoneNumber { get; set; }        
    }
}
