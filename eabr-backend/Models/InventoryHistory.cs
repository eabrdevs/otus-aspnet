﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eabr_backend.Models
{    
    //история эксплуатации инвентаря
    public class InventoryHistory: EntityBase
    {
        [Required]
        [Display(Name = "Дата выдачи")]
        public DateTime DateStart { get; set; }
        [Display(Name = "Дата возврата")]
        public DateTime DateEnd { get; set; }
        [Required]
        [Display(Name = "Выдано пользователю")]
        public Guid InventoryByUserId { get; set; }
        //public User User { get; set; }
        [Required]
        [Display(Name = "Инвентарь")]
        public Guid InventoryByItemId { get; set; }        
        public InventoryItem InventoryByItem { get; set; }
        [Display(Name = "Подтверждение получения")]
        public bool IsConfirmed { get; set; }
        [Display(Name = "Примечание")]
        public string Remark { get; set; }
    }
}
