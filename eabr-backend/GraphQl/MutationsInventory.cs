﻿using eabr_backend.Data;
using eabr_backend.Data.Inventory;
using eabr_backend.GraphQl.Inventories;
using eabr_backend.GraphQl.InventoryHistories;
using eabr_backend.Models;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.GraphQl
{
    public class MutationsInventory
    {        
        [UseFiltering()]
        [UseSorting()]
        [Authorize(Roles = new[] { "admin" })]
        public async Task<AddInventoryPayload> AddInventoryItemAsync(AddInventoryInput input, [Service] IInventoryRepository inventoryRepository)
        {
            var inventoryItem = new InventoryItem
            {
                Name = input.Name,
                Description = input.Description,
                CreatedById = input.CreatedById                
            };
            await inventoryRepository.CreateAsync(inventoryItem);            
            return new AddInventoryPayload(inventoryItem);
        }

        [UseFiltering()]
        [UseSorting()]
        [Authorize(Roles = new[] { "admin" })]
        public async Task<AddInventoryHistoryPayload> AddInventoryHistoryAsync(AddInventoryHistoryInput input, [Service] IInventoryHistoryRepository inventoryHistoryRepository)
        {
            var history = new InventoryHistory
            {
                DateStart = DateTime.Now,
                DateEnd = new DateTime(),
                InventoryByUserId = input.InventoryByUserId,
                InventoryByItemId = input.InventoryByItemId,
                IsConfirmed = false,
                Remark = input.Remark
            };
            await inventoryHistoryRepository.CreateAsync(history);
            return new AddInventoryHistoryPayload(history);
        }
        
        [UseFiltering()]
        [UseSorting()]
        [Authorize]        
        public async Task<AddInventoryHistoryPayload> Confirmed(ConfirmedInput input, [Service] IInventoryHistoryRepository inventoryHistoryRepository)
        {
            var updhst = await inventoryHistoryRepository.Confirmed(input);
            return new AddInventoryHistoryPayload(updhst);
        }

    }
}
