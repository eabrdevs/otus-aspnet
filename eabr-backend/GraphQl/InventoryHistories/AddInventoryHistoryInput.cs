﻿using System;

namespace eabr_backend.GraphQl.InventoryHistories
{
    public record AddInventoryHistoryInput(Guid InventoryByUserId, Guid InventoryByItemId, string Remark);
}
