﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.GraphQl.InventoryHistories
{
    public record ConfirmedInput(Guid Id, bool IsConfirmed);
}
