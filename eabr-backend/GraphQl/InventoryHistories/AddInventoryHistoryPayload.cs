﻿using eabr_backend.Models;

namespace eabr_backend.GraphQl.InventoryHistories
{
    public record AddInventoryHistoryPayload(InventoryHistory history);
}
