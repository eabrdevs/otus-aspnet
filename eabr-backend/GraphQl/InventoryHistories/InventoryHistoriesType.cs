﻿using eabr_backend.Data;
using eabr_backend.Data.IdentityUser;
using eabr_backend.Models;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.GraphQl.Inventories
{    
    public class InventoryHistoriesType : ObjectType<InventoryHistory>
    {
        protected override void Configure(IObjectTypeDescriptor<InventoryHistory> descriptor)
        {
            descriptor.Description("История инвентаря");
            descriptor
                .Field(i => i.Id)
                .Description("Уникальный идентификатор");
            descriptor
                .Field(i => i.DateStart)
                .Description("Дата выдачи");
            descriptor
                .Field(i => i.DateEnd)
                .Description("Дата возврата");
            descriptor
                .Field(i => i.InventoryByUserId)
                .Description("Выдано пользователю");
            descriptor
                .Field(i => i.InventoryByItemId)
                .Description("Инвентарь");
            descriptor
                .Field(i => i.IsConfirmed)                
                .Description("Подтверждение получения");
            descriptor
                .Field(i => i.Remark)
                .Description("Примечание");
            descriptor
                .Field("usersInventory")
                .ResolveWith<Resolvers>(i => i.UsersInventoryAsync(default!, default!))
                .Description("Данные пользователя");
        }

        private class Resolvers
        {
            [UseProjection]
            public async Task<IQueryable<User>> UsersInventoryAsync([Parent] InventoryHistory item, [Service] IUserRepository userRepository)
            {
                var id = item.InventoryByUserId.ToString();
                var user = await userRepository.GetByIdAsync(id);
                return user;
            }
        }

    }
}