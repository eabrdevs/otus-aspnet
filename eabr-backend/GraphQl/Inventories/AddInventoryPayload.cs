﻿using eabr_backend.Models;

namespace eabr_backend.GraphQl.Inventories
{
    public record AddInventoryPayload(InventoryItem inventoryItem);
}
