﻿using System;

namespace eabr_backend.GraphQl.Inventories
{
    public record AddInventoryInput(string Name, string Description, Guid CreatedById);
}
