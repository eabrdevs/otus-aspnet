﻿using eabr_backend.Data;
using eabr_backend.Data.IdentityUser;
using eabr_backend.Models;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using System.Linq;
using System.Threading.Tasks;

namespace eabr_backend.GraphQl.Inventories
{    
    public class InventoryItemType : ObjectType<InventoryItem>
    {
        protected override void Configure(IObjectTypeDescriptor<InventoryItem> descriptor)
        {
            descriptor.Description("Инвентарь");
            descriptor
                .Field(i => i.Id)
                .Description("Уникальный идентификатор");
            descriptor
                .Field(i => i.Code)
                .Description("Инвентарный номер");
            descriptor
                .Field(i => i.Name)
                .Description("Наименование");
            descriptor
                .Field(i => i.Description)
                .Description("Описание");
            descriptor
                .Field(i => i.CreatedById)
                .Description("ID Пользователя создавшего инвентарь");
            descriptor
                .Field(i => i.InventoryHistories)                
                .Description("История инвенторя");            
            descriptor
                .Field("createUser")
                .ResolveWith<Resolvers>(i => i.UserCreateAsync(default!, default!))
                .Description("Данные пользователя создавшего инвентарь");
        }

        private class Resolvers
        {
            [UseProjection]
            public async Task<IQueryable<User>> UserCreateAsync([Parent] InventoryItem item, [Service] IUserRepository userRepository)
            {                
                var user = await userRepository.GetByIdAsync(item.CreatedById.ToString());
                return user;
            }
        }

    }
}