﻿using eabr_backend.Data;
using eabr_backend.Data.IdentityUser;
using eabr_backend.Data.Inventory;
using eabr_backend.Models;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Data;
using HotChocolate.Types;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace eabr_backend.GraphQl
{
    public class Queries
    {
        [UseProjection]
        [UseFiltering()]
        [UseSorting()]
        [Authorize(Roles = new[] { "admin" })]
        public async Task<IQueryable<InventoryItem>> AllInventories([Service] IInventoryRepository inventoryRepository)
        {
            return await inventoryRepository.GetInventoriesAsync();
        }

        [UseProjection]
        [UseFiltering()]
        [UseSorting()]
        [Authorize(Roles = new[] { "admin" })]
        public async Task<IQueryable<InventoryHistory>> InventoriesHistories([Service] IInventoryHistoryRepository inventoryHistoryRepository)
        {
            return await inventoryHistoryRepository.GetInventoryHistoriesAsync();
        }

        #region Авторизация
        [Authorize(Roles = new[] {"admin"})]
        [Authorize]
        public bool AuthorizeQuery([Service] IHttpContextAccessor context, ClaimsPrincipal claimsPrincipal)
        {
            var user = context.HttpContext.User;
            var username = user.FindFirstValue("preferred_username");
            return true;
        }
        #endregion
        
        [UseProjection]
        [UseFiltering()]
        [UseSorting()]
        [Authorize(Roles = new[] { "admin" })]
        public async Task<IQueryable<User>> AllUsers([Service] IHttpContextAccessor context, ClaimsPrincipal claimsPrincipal, [Service] IUserRepository userRepository) =>
            await userRepository.GetUserAsync();
    }    
}
