using eabr_backend.Data;
using eabr_backend.Data.IdentityUser;
using eabr_backend.Data.Inventory;
using eabr_backend.Extensions;
using eabr_backend.GraphQl;
using HotChocolate.AspNetCore;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using GraphQL.Server.Ui.Voyager;
using eabr_backend.GraphQl.Inventories;

namespace eabr_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public class HttpRequestInterceptor: DefaultHttpRequestInterceptor
        {
            public override ValueTask OnCreateAsync(HttpContext context, 
                IRequestExecutor requestExecutor, IQueryRequestBuilder requestBuilder, 
                CancellationToken cancellationToken)
            {
                var identity = new ClaimsIdentity();
                var rolesv = context.User.FindFirstValue("realm_access");
                if (rolesv != null)
                {
                    dynamic roles = Newtonsoft.Json.Linq.JObject.Parse(rolesv);
                    foreach (var r in roles.roles)
                        identity.AddClaim(new Claim(ClaimTypes.Role, r.value));
                }

                var namev = context.User.FindFirstValue("preferred_username");
                if (namev != null)
                    identity.AddClaim(new Claim(ClaimTypes.Name, namev));

                context.User.AddIdentity(identity);
                return base.OnCreateAsync(context, requestExecutor, requestBuilder, cancellationToken);
            }
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSwaggerExtension();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, config =>
                {
                    config.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromSeconds(5),
                        ValidateAudience = false
                    };
                    config.Authority = "https://localhost:44310/";
                    config.Audience = "app.api.employeeprofile";
                    //config.RequireHttpsMetadata = false;
                });            
            #region ��������� �����������
            services.AddLogging(loggingBuilder =>
            {
                //����� � �������
                loggingBuilder
                      .AddConsole()
                      //������� ������� SQL
                      .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information);

                //������� � ���� �������
                loggingBuilder
                      .AddDebug();
            });
            #endregion
            //CORS            
            services.AddCorsExtension();
            services.AddHealthChecks();
            //JSON
            services.AddControllersWithViews().AddNewtonsoftJson(option =>
            option.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddNewtonsoftJson(option => option.SerializerSettings.ContractResolver
                = new DefaultContractResolver());

            services.AddHttpContextAccessor();
            //GrapQl
            services.AddMemoryCache();
            services
                .AddGraphQLServer()
                .AddHttpRequestInterceptor<HttpRequestInterceptor>()
                .AddAuthorization()
                .AddQueryType<Queries>()
                .AddMutationType<MutationsInventory>()
                .AddType<InventoryItemType>()
                .AddType<InventoryHistoriesType>()                
                //.AddType<UserExtension>()
                .AddProjections()
                .AddFiltering()
                .AddSorting()
                .UseAutomaticPersistedQueryPipeline()
                .AddInMemoryQueryStorage();

            services.AddDbContext<DataContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("Inventories")), ServiceLifetime.Transient);
            services.AddDbContext<UserContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Users")), ServiceLifetime.Transient);
            //services.AddPooledDbContextFactory<DataContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("Inventories")));
            //services.AddPooledDbContextFactory<UserContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Users")));

            services.AddControllers();

            services.AddTransient<IInventoryRepository, InventoryRepository>();
            services.AddTransient<IInventoryHistoryRepository, InventoryHistoryRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            //services.AddScoped<JwtService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCors("AllowWebEabr");

            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseSwaggerExtension();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGraphQL("/graphql");
            });

            app.UseGraphQLVoyager(new GraphQLVoyagerOptions()
            {
                GraphQLEndPoint = "/graphql",
                Path = "/graphql-voyager"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Photos")),
                RequestPath = "/Photos"
            });
        }
    }
}
