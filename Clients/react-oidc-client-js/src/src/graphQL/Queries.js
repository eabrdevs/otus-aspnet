import { gql } from '@apollo/client';


export const LOAD_USERS = gql `
    query {
        allUsers {
            id
            email
            userName
            phoneNumber
        }
    }
`
export const LOAD_INVENTORIES = gql `
    query {
        allInventories {
            code
            name
            description
            inventoryHistories {
                dateStart
                dateEnd
                isConfirmed
                inventoryByUserId
                usersInventory {
                    id
                    userName
                    email
                    phoneNumber
                }
            }
            createdById
            createUser {
                id
                userName
                email
                phoneNumber
            }
        }
    }
`;