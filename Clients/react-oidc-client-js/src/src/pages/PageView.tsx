import * as React from 'react';
import { clickButton } from "../components/AppContent";
import GetInventories from "../components/GetInventories";
import GetUsers from "../components/GetUsers";
import JsonTreeViewer from "../components/JsonTreeViewer";

export interface IClickButtonProps {
  clickButton: any;
  userInfo: any;
}

export default class PageView extends React.Component<IClickButtonProps, any> {
  public shouldExpandNode = (keyPath: Array<string | number>, data: [any] | {}, level: number) => {
    return true;
  };

  renderSwitch(param: any) {
    switch(param) {
      case clickButton.Users:        
        return <GetUsers/>;
      case clickButton.Inventories:
        return <GetInventories getInventories="allInventories"/>
      case clickButton.UserInfo:
        return <JsonTreeViewer data={this.props.userInfo} title="User Profile" shouldExpandNode={this.shouldExpandNode}/>
      default:
        return '';
    }
  }  public render() {
    return (      
      <div className="row">
        <div className="col-md-6">
          {this.renderSwitch(this.props.clickButton)}
        </div>
      </div>
    );
  }
}