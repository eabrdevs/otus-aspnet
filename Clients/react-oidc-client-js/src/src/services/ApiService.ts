import axios from 'axios';
import { AuthService } from './AuthService';

export class ApiService {
  private authService: AuthService;
  constructor() {
    this.authService = new AuthService();
  }

  public callApi(methodApi: string): Promise<any> {
    return this.authService.getUser().then(user => {
      if (user && user.access_token) {
        return this._callApi(user.access_token, methodApi).catch(error => {
          if (error.response.status === 401) {
            return this.authService.renewToken().then(renewedUser => {
              return this._callApi(renewedUser.access_token, methodApi);
            });
          }
          throw error;
        });
      } else if (user) {
        return this.authService.renewToken().then(renewedUser => {
          return this._callApi(renewedUser.access_token, methodApi);
        });
      } else {
        throw new Error('user is not logged in');
      }
    });
  }

  private _callApi(token: string, methodApi: string) {    
    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: 'Bearer ' + token
    };
    return axios.get(process.env.REACT_APP_API_ENDPOINT + methodApi, { headers });
  }
}
