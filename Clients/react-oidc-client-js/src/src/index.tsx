import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

import 'react-app-polyfill/ie11';
import 'core-js';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './containers/App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    HttpLink,
    from
} from '@apollo/client';
import { onError } from "@apollo/client/link/error";
import { AuthService } from './services/AuthService';

function doToken() {
  const authService = new AuthService();
  return new Promise((access_token, failure) => {
    authService.getUser()
    .then(
      user=> {
        if (user && user.access_token) {
          access_token(user.access_token.toString())
        } else if (user) {
          authService.renewToken().then(
            renewedUser => {
              access_token(renewedUser.access_token.toString());
            });
          }
          else {failure("Ошибка")}
        })
    .catch(() => failure("Ошибка"));
    })
}

const getToken = doToken().then(token=>{
  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.forEach(({ message, locations, path }) =>
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        )
      );
    if (networkError) console.log(`[Network error]: ${networkError}`);
  });

  const link = from([
  errorLink,
  new HttpLink({uri: process.env.REACT_APP_GRAPHQL,
                headers: {
                  Accept: 'application/json',
                  Authorization: 'Bearer '+ token
                }
              })
  ])
  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: link  
    });

    return ReactDOM.render(
    <ApolloProvider client={client}>
      <App />      
    </ApolloProvider>
    , document.getElementById('root') as HTMLElement);
}).catch(()=> {return ReactDOM.render(
                      <App />
                      , document.getElementById('root') as HTMLElement);});
getToken.then();
registerServiceWorker();
