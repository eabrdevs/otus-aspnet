import React, {useEffect, useState} from "react";
import {useQuery} from '@apollo/client';
import {LOAD_INVENTORIES} from '../graphQL/Queries';

function GetInventories(props) {
    const {error, loading, data} = useQuery(LOAD_INVENTORIES)
    //console.log(error);
    const [inventories, setInventories] = useState([]);
    const [getInventories, setGetInventories] = useState(props.getInventories);

    useEffect(() => {
        if (data){
            setInventories(data.allInventories);
        }        
    }, [data]);
    
    function handleChange(event) {        
        setGetInventories(event.target.value);        
    };

    return (
        <div>
            <select value={getInventories} onChange={handleChange}>
                <option value="issuedInventories">Выданный инвентарь</option>
                <option value="allInventories">Весь инвентарь</option>                
            </select>            
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Наименование</th>
                        <th>Описание</th>
                        <th>Создал</th>
                        {getInventories == "issuedInventories" &&
                        <th>Выдан пользователю</th>
                        }
                    </tr>
                </thead>
                <tbody>
                {inventories                
                .filter(value => (value.inventoryHistories.length > 0 && getInventories == "issuedInventories") || getInventories == "allInventories")
                .map((value, idx) => {
                    return <tr key={idx}>
                                <td>{value.code}</td>                                
                                <td>{value.name}</td>
                                <td>{value.description}</td>                                
                                {value.createUser
                                .map((user, idx) => (
                                <td key={idx}>
                                    <div>{user.userName}</div>
                                    <div>{user.email}</div>
                                </td>
                                ))}
                                {value.inventoryHistories
                                .filter(value => value.usersInventory.length > 0 && getInventories == "issuedInventories")
                                .map((history, idx) => (
                                <td key={idx}>
                                    <div><label>Подтверждение получения <input type="checkbox" checked={history.isConfirmed} readOnly={true}></input></label></div>
                                    <div><label>Дата выдачи {(new Date(history.dateStart)).toLocaleDateString()}</label></div>
                                    <div><label>Дата возврата {history.dateEnd? (new Date(history.dateEnd)).toLocaleDateString():null}</label></div>
                                    <div>
                                        {history.usersInventory
                                        .map((issuedUser, idx) => (
                                            <div key={idx}>
                                                <div>{issuedUser.userName}</div>
                                                <div>{issuedUser.email}</div>
                                                <div>{issuedUser.phoneNumber}</div>
                                            </div>
                                        ))}                                                                                
                                    </div>
                                </td>
                                ))}
                                <td>

                                </td>
                           </tr>;
                })}
                </tbody>
            </table>
        </div>
    );
}

export default GetInventories;