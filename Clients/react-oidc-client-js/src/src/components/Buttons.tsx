import * as React from 'react';

interface IButtonsProps {
  login: () => void;
  getUser: () => void;
  inventories: () => void;
  users: () => void;
  //renewToken: () => void;
  logout: () => void;
}

const Buttons: React.SFC<IButtonsProps> = props => {
  return (
    <div className="row">
      <div className="col-md-12 text-center" style={{ marginTop: '30px' }}>
        <button className="btn btn-primary"  style={{ margin: '10px' }} onClick={props.login}>
          Login
        </button>
        <button className="btn btn-warning" style={{ margin: '10px' }} onClick={props.users}>
          Пользователи
        </button>
        <button className="btn btn-warning" style={{ margin: '10px' }} onClick={props.inventories}>
          Инвентарь
        </button>
        <button className="btn btn-secondary" style={{ margin: '10px' }} onClick={props.getUser}>
          Get User info
        </button>                        
        <button className="btn btn-dark" style={{ margin: '10px' }} onClick={props.logout}>
          Logout
        </button>
      </div>
    </div>
  );
};

export default Buttons;
