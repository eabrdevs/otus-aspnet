import * as React from 'react';

import { ToastContainer, toast } from 'react-toastify';
import PageView from '../pages/PageView';

import { ApiService } from '../services/ApiService';
import { AuthService } from '../services/AuthService';

import AuthContent from './AuthContent';
import Buttons from './Buttons';

export enum clickButton{
  Users,
  Inventories,
  UserInfo
}
export default class AppContent extends React.Component<any, any> {
  public authService: AuthService;
  public apiService: ApiService;
  private shouldCancel: boolean;

  constructor(props: any) {
    super(props);

    this.authService = new AuthService();
    this.apiService = new ApiService();
    this.state = { user: {}, inventory: {}, clickButton: clickButton};
    this.shouldCancel = false;
  }

  public componentDidMount() {
    this.getUser();
  }

  public login = () => {
    this.authService.login();
  };

  public users = () => {    
    this.setState({ clickButton: clickButton.Users });
  };

  public inventories = () => {
    /*this.apiService
      .callApi('Inventory/get')
      .then(data => {
        this.setState({ inventory: data.data });
        toast.success('Inventory return successfully data, check in section - Inventory response');
      })
      .catch(error => {
        toast.error(error);
      });*/
      this.setState({ clickButton: clickButton.Inventories });
  };

  public componentWillUnmount() {
    this.shouldCancel = true;
  }

  public renewToken = () => {
    this.authService
      .renewToken()
      .then(user => {
        toast.success('Token has been sucessfully renewed. :-)');
        this.getUser();
      })
      .catch(error => {
        toast.error(error);
      });
  };

  public logout = () => {
    this.authService.logout();
  };

  public getUser = () => {
    this.authService.getUser().then(user => {
      if (user) {        
        toast.success('User has been successfully loaded from store.');
      } else {
        toast.info('You are not logged in.');
      }

      if (!this.shouldCancel) {
        this.setState({ user });
      }
    });    
  };
  public getUserInfo = () => {
    this.setState({ clickButton: clickButton.UserInfo });
  }
  public render() {
    const isLoggedIn =this.state.user;
    return (
      <>
        <ToastContainer />

        <Buttons
          login={this.login}          
          logout={this.logout}
          //renewToken={this.renewToken}
          getUser={this.getUserInfo}
          inventories={this.inventories}
          users={this.users}
        />
        
        {isLoggedIn? <PageView clickButton={this.state.clickButton} userInfo={this.state.user}/>:null}
        {/* <AuthContent api={this.state.inventories} user={this.state.user}/> */}
      </>
      
    );
  }
}
