import React, {useEffect, useState} from "react";
import {useQuery} from '@apollo/client';
import {LOAD_USERS} from '../graphQL/Queries';

function GetUsers() {
    const {error, loading, data} = useQuery(LOAD_USERS)
    console.log(error);
    const [users, setUsers] = useState([])
    useEffect(() => {
        if (data){
            setUsers(data.allUsers);
        }        
    }, [data])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>ФИО</th>
                        <th>Емайл</th>
                        <th>Телефон</th>
                    </tr>
                </thead>
                <tbody>
                {users.map((value) => {
                    return <tr key={value.id}>
                                <td>{value.id}</td>                                
                                <td>{value.userName}</td>
                                <td>{value.email}</td>
                                <td>{value.phoneNumber}</td>
                                <td>

                                </td>
                           </tr>;
                })}
                </tbody>
            </table>
        </div>
    );
}

export default GetUsers;